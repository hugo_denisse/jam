# Jam

Web application made in the memory of the late Jamlegend.

(and also to train my skills with [React](https://reactjs.org/), [Redux](https://redux.js.org/) and [Redux-Saga](https://redux-saga.js.org/))

You can view it in action [here](http://jam.hugod.fr).

It was created with the `create-react-app` tool.

## Installation

Node and Npm are required.

```sh
git clone https://bitbucket.org/hugo_denisse/jam
cd jam
npm install
```

To launch the development server, type:

```sh
npm start
```

## Development server

Default port: `3000`.

By default, the development server will proxy requests it can not handle (ie: api requests) to port `3001`.

Make sure to run the [jam-api](https://bitbucket.org/hugo_denisse/jam-api/) server on that port on your marchine.

## Usefull libraries

[PixiJS](https://github.com/pixijs) as the rendering engine.

[MIDIFile](https://github.com/nfroidure/MIDIFile) and [MIDIEvents](https://github.com/nfroidure/MIDIEvents) for the decoding of midi files.

[soundfont-player](https://github.com/danigb/soundfont-player) to play midi sounds (soundfounts) in the browser.
