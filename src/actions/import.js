export const IMPORT_MIDI_REQUEST = "IMPORT_MIDI_REQUEST";
export const IMPORT_MIDI_SUCCESS = "IMPORT_MIDI_SUCCESS";
export const IMPORT_MIDI_ERROR = "IMPORT_MIDI_ERROR";
export const IMPORT_MIDI_OPTIONS = "IMPORT_MIDI_OPTIONS";
export const IMPORT_MIDI_RESET = "IMPORT_MIDI_RESET";

export const importMidiRequest = file => {
  return {
    type: IMPORT_MIDI_REQUEST,
    file
  };
};

export const importMidiSuccess = (midiBuffer, midiInfos) => {
  return {
    type: IMPORT_MIDI_SUCCESS,
    midiBuffer,
    midiInfos
  };
};

export const importMidiError = error => {
  return {
    type: IMPORT_MIDI_ERROR,
    error
  };
};

export const importMidiOptions = midiOptions => {
  return {
    type: IMPORT_MIDI_OPTIONS,
    midiOptions
  };
};

export const importMidiReset = () => {
  return {
    type: IMPORT_MIDI_RESET
  };
};
