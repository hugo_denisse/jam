export const LOGIN_WITH_PASSWORD_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const REGISTER_REQUEST = "REGISTER_REQUEST";
export const LOGOUT = "LOGOUT";
export const USER_INFO = "USER_INFO";

export const loginWithPasswordRequest = (email, password) => {
  return {
    type: LOGIN_WITH_PASSWORD_REQUEST,
    email,
    password
  };
};

export const loginSuccess = (access_token, refresh_token) => {
  return {
    type: LOGIN_SUCCESS,
    access_token,
    refresh_token
  };
};

export const loginError = error => {
  return {
    type: LOGIN_ERROR,
    error
  };
};

export const registerRequest = (name, email, password) => {
  return {
    type: REGISTER_REQUEST,
    name,
    email,
    password
  };
};

export const logout = cause => {
  return {
    type: LOGOUT,
    cause
  };
};

export const userInfo = infos => {
  return {
    type: USER_INFO,
    infos
  };
};
