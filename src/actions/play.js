export const PLAY_SONG_ERROR = "PLAY_SONG_ERROR";

export const playSongError = error => {
  return {
    type: PLAY_SONG_ERROR,
    error
  };
};

export const FETCH_SONG_META_REQUEST = "FETCH_SONG_META_REQUEST";
export const FETCH_SONG_META_RESPONSE = "FETCH_SONG_META_RESPONSE";
export const FETCH_SONG_META_ERROR = "FETCH_SONG_META_ERROR";

export const fetchSongMetaRequest = name => {
  return {
    type: FETCH_SONG_META_REQUEST,
    name
  };
};

export const fetchSongMetaResponse = data => {
  return {
    type: FETCH_SONG_META_RESPONSE,
    data
  };
};

export const fetchSongMetaError = error => {
  return {
    type: FETCH_SONG_META_ERROR,
    error
  };
};
