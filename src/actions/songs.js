export const FETCH_SONGS_LIST_REQUEST = "FETCH_SONGS_LIST_REQUEST";
export const FETCH_SONGS_LIST_RESPONSE = "FETCH_SONGS_LIST_RESPONSE";
export const FETCH_SONGS_LIST_ERROR = "FETCH_SONGS_LIST_ERROR";

export const fetchSongsListRequest = filter => {
  return {
    type: FETCH_SONGS_LIST_REQUEST,
    filter
  };
};

export const fetchSongsListResponse = data => {
  return {
    type: FETCH_SONGS_LIST_RESPONSE,
    data
  };
};

export const fetchSongsListError = error => {
  return {
    type: FETCH_SONGS_LIST_ERROR,
    error
  };
};
