import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import Error from "../components/Error";
import { registerRequest, loginError } from "../actions/login";
import { Link } from "react-router-dom";

class Register extends Component {
  componentDidMount() {
    this.props.dispatch(loginError(null));
  }

  onSubmit(evt) {
    evt.preventDefault();

    if (!this.inputName.checkValidity()) {
      this.props.dispatch(
        loginError({
          message:
            "Selectionnez un pseudo d'au moins 3 caractères ne contenant que des caractères alphanumériques"
        })
      );
      return false;
    }
    if (!this.inputEmail.checkValidity()) {
      this.props.dispatch(
        loginError({ message: "Votre adresse mail doit être un email valide." })
      );
      return false;
    }
    if (!this.inputPassword.checkValidity()) {
      this.props.dispatch(
        loginError({
          message: "Votre mot de passe doit faire au moins 6 caractères"
        })
      );
      return false;
    }

    this.props.dispatch(
      registerRequest(
        this.inputName.value,
        this.inputEmail.value,
        this.inputPassword.value
      )
    );
    return false;
  }

  render() {
    return (
      <div>
        <h2>Inscription</h2>
        <form action="#">
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Pseudo"
              ref={elem => (this.inputName = elem)}
              minLength="3"
              pattern="[a-zA-Z0-9]+"
              required
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="email"
              placeholder="Email"
              ref={elem => (this.inputEmail = elem)}
              required
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              placeholder="Mot de passe"
              ref={elem => (this.inputPassword = elem)}
              minLength="6"
              required
            />
          </div>
          <input
            type="submit"
            value="S'inscrire"
            className="btn btn-dark"
            onClick={evt => this.onSubmit(evt)}
          />
        </form>
        {this.props.error ? <Error error={this.props.error} /> : null}
        {this.props.logged ? <Redirect to="/" /> : null}
        <p style={{ marginTop: 30 }}>
          Vous avez déjà un compte ? <Link to="/login">Se connecter</Link>
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  let error = state.login.error;
  if (error) {
    if (error.status === 409) {
      error = { message: "Email ou pseudo déjà utilisé." };
    } else if (error.json && error.json.error === "ValidationError") {
      error = { message: "Certains champs saisis ont une valeur incorrecte." };
    }
  }

  return {
    error,
    logged: !!state.login.access_token
  };
};

export default connect(mapStateToProps)(Register);
