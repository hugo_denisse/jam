import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import Error from "../components/Error";
import { loginWithPasswordRequest, loginError } from "../actions/login";
import { Link } from "react-router-dom";

class Login extends Component {
  componentDidMount() {
    this.props.dispatch(loginError(null));
  }

  onSubmit(evt) {
    evt.preventDefault();

    if (!this.inputEmail.checkValidity()) {
      this.props.dispatch(
        loginError({ message: "Votre adresse mail doit être un email valide." })
      );
      return false;
    }
    if (!this.inputPassword.checkValidity()) {
      this.props.dispatch(
        loginError({ message: "Veuillez saisir votre mot de passe" })
      );
      return false;
    }

    this.props.dispatch(
      loginWithPasswordRequest(this.inputEmail.value, this.inputPassword.value)
    );
    return false;
  }

  render() {
    return (
      <div>
        <h2>Connexion</h2>
        <form action="#">
          <div className="form-group">
            <input
              className="form-control"
              type="email"
              placeholder="Email"
              ref={elem => (this.inputEmail = elem)}
              required
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              placeholder="Mot de passe"
              ref={elem => (this.inputPassword = elem)}
              required
            />
          </div>
          <input
            type="submit"
            value="Connexion"
            className="btn btn-dark"
            onClick={evt => this.onSubmit(evt)}
          />
        </form>
        {this.props.error ? <Error error={this.props.error} /> : null}
        {this.props.logged ? <Redirect to="/" /> : null}
        <p style={{ marginTop: 30 }}>
          Pas encore de compte ? <Link to="/register">En créer un</Link>
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  let error = state.login.error;
  if (error) {
    if (error.status === 401) {
      error = { message: "Email ou mot de passe incorrect." };
    } else if (error.json && error.json.error === "ValidationError") {
      error = { message: "Certains champs saisis ont une valeur incorrecte." };
    }
  }

  return {
    error,
    logged: !!state.login.access_token
  };
};

export default connect(mapStateToProps)(Login);
