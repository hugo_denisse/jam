import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchSongMetaRequest, playSongError } from "../actions/play";
import Game from "../lib/game";
import Error from "../components/Error";
import Api from "../lib/api";

class Play extends Component {
  componentDidMount() {
    const songname = this.props.match ? this.props.match.params.songname : null;
    if (songname) {
      /**
       * si on arrive ici par nom de chanson
       */
      this.props.dispatch(fetchSongMetaRequest(songname));
    } else if (this.props.midi) {
      /**
       * si on arrive ici par importer
       */
      Game.start(
        this.gameDiv,
        {
          midiBuffer: this.props.midi,
          channel: this.props.channel || "*"
        },
        {
          backgroundColor: 0x1c2022
        }
      );
    }
  }

  componentWillUnmount() {
    Game.stop();
    // on supprime les éventuelles erreurs
    this.props.dispatch(playSongError(null));
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.isFetching === false &&
      nextProps.error === null &&
      nextProps.song
    ) {
      Game.start(
        this.gameDiv,
        {
          midi: "/songs/" + nextProps.song.midi,
          mp3: "/songs/" + nextProps.song.mp3,
          delay: nextProps.song.delay || 0,
          channel: nextProps.song.channel || "*"
        },
        {
          backgroundColor: 0x1c2022,
          onComplete: infos => this._onPlayComplete(infos)
        }
      );
    }
  }

  _replay() {
    Game.restart();
  }

  _onPlayComplete(infos) {
    Api.post("score", {
      song: this.props.song.uuid,
      score: infos.score,
      streak: infos.streak,
      percent: infos.notesHit / infos.notesTotal
    });
  }

  render() {
    return (
      <div>
        {this.props.error ? <Error error={this.props.error} /> : null}
        <p style={{ marginTop: 10 }}>Info : Utilisez les touches A Z E R T</p>
        {this.props.isFetching ? <p>Chargement de la chanson...</p> : null}
        <div id="game" ref={game => (this.gameDiv = game)} />
        <button className="btn btn-dark" onClick={() => this._replay()}>
          Rejouer
        </button>
      </div>
    );
  }
}

Play.propTypes = {
  midi: PropTypes.object,
  channel: PropTypes.any
};

const mapStateToProps = state => {
  return {
    isFetching: state.play.isFetching,
    song: state.play.song,
    error: state.play.error
  };
};

export default connect(mapStateToProps)(Play);
