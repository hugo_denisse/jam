import React, { Component } from "react";
import {
  importMidiRequest,
  importMidiOptions,
  importMidiReset
} from "../actions/import";
import Play from "../containers/Play";
import { connect } from "react-redux";
import Error from "../components/Error";
import ChannelsList from "../components/ChannelsList";

class Import extends Component {
  componentDidMount() {
    this.inputElement.click();
  }

  componentWillUnmount() {
    this.props.dispatch(importMidiReset());
  }

  render() {
    return (
      <div>
        <label htmlFor="input-file" className="btn btn-dark">
          Importer un fichier
        </label>
        <input
          id="input-file"
          ref={input => (this.inputElement = input)}
          onChange={e => {
            if (e.target.files && e.target.files.length) {
              this.props.dispatch(importMidiRequest(e.target.files[0]));
            }
          }}
          type="file"
          style={style.inputFile}
        />
        {this.props.midiBuffer &&
        this.props.midiInfos &&
        !this.props.midiOptions &&
        !this.props.error ? (
          <div>
            <div style={style.form}>
              <ChannelsList
                ref={ref => (this.channelsList = ref)}
                channels={this.props.midiInfos.channels}
              />
            </div>
            <button
              className="btn btn-dark"
              onClick={event =>
                this.channelsList.selected.length > 0
                  ? this.props.dispatch(
                      importMidiOptions({
                        channel: this.channelsList.selected
                      })
                    )
                  : null
              }
            >
              Générer la partition
            </button>
          </div>
        ) : null}
        {this.props.error ? <Error error={this.props.error} /> : null}
        {this.props.midiBuffer && this.props.midiOptions ? (
          <Play
            midi={this.props.midiBuffer}
            channel={this.props.midiOptions.channel}
          />
        ) : null}
      </div>
    );
  }
}

const style = {
  inputFile: {
    display: "none"
  },
  form: {
    marginTop: 10,
    marginBottom: 15
  }
};

const mapStateToProps = state => {
  return {
    error: state.import.error,
    midiBuffer: state.import.midiBuffer,
    midiInfos: state.import.midiInfos,
    midiOptions: state.import.midiOptions
  };
};

export default connect(mapStateToProps)(Import);
