import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../actions/login";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="navbar">
          <div className="container">
            {this.props.logged ? (
              <div className="right">
                Hello {this.props.username}{" "}
                <Link
                  to="/"
                  className="btn btn-dark"
                  onClick={() => {
                    this.props.dispatch(logout("User logged out"));
                  }}
                >
                  Se déconnecter
                </Link>
              </div>
            ) : (
              <div className="right">
                <Link to="/login" className="btn btn-dark">
                  Connexion
                </Link>
              </div>
            )}
          </div>
        </div>
        <Link to="/">
          <h1>La Légende de Jam</h1>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    logged: !!state.login.access_token,
    username: state.login.name
  };
};

export default connect(mapStateToProps)(Header);
