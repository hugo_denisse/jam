import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchSongsListRequest } from "../actions/songs";
import Error from "../components/Error";

class Menu extends Component {
  componentDidMount() {
    if (!this.props.songs) {
      this.props.dispatch(fetchSongsListRequest());
    }
  }

  render() {
    return (
      <div>
        {this.props.songs && this.props.songs.length ? (
          <ul id="menu" style={style.ul}>
            <li style={style.li}>
              <Link to="/import" className="btn btn-dark">
                Importer un fichier MIDI
              </Link>
            </li>
            <li style={style.or}>ou jouer une chanson existante</li>
            {this.props.songs.map((song, index) => {
              return (
                <li key={index} style={style.li}>
                  <Link to={`/play/${song.name}`} className="btn btn-dark">
                    {song.name}
                  </Link>
                </li>
              );
            })}
          </ul>
        ) : (
          <ul id="menu" style={style.ul}>
            <li style={style.li}>
              <Link to="/import" className="btn btn-dark">
                Importer un fichier MIDI
              </Link>
            </li>
          </ul>
        )}
        {this.props.error ? <Error error={this.props.error} /> : null}
      </div>
    );
  }
}

const style = {
  ul: { listStyleType: "none", padding: 0 },
  li: {
    paddingBottom: 10
  },
  or: {
    paddingBottom: 10,
    marginTop: 15,
    marginBottom: 15
  }
};

const mapStateToProps = state => {
  return {
    isFetching: state.songs.isFetching,
    songs: state.songs.list ? state.songs.list.songs : undefined,
    error: state.songs.error
  };
};

export default connect(mapStateToProps)(Menu);
