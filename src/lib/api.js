import jwt_decode from "jwt-decode";
import co from "co";
import { logout, loginSuccess, userInfo } from "../actions/login";

let store;
let accessToken,
  refreshToken,
  accessExp = 0;

/**
 * fait une requête GET vers l'API
 *
 * @param {string} route
 * @param {object} options
 */
export const get = (route, options) => {
  return callApi(route, "GET", undefined, options);
};

/**
 * fait une requête POST vers l'API
 *
 * @param {string} route
 * @param {object} body
 * @param {object} options
 */
export const post = (route, body, options) => {
  return callApi(route, "POST", body, options);
};

/**
 * fait une demande d'obtention d'un nouvel access_token (en envoyant le refresh_token vers l'API d'authentication)
 *
 * @return Promise qui retourne true en cas de succes, false sinon
 */
export const refreshAccessToken = () => {
  return post(
    "auth/refresh",
    { token: refreshToken },
    { useAccessToken: false }
  )
    .then(result => {
      // on a bien récupéré un nouvel access token
      store.dispatch(loginSuccess(result.access_token, refreshToken));
      return true;
    })
    .catch(err => {
      store.dispatch(logout(err.statusText || "Could not refresh"));
      return false;
    });
};

/**
 * fait un appel à l'API
 *
 * @param {string} route route à appeler
 * @param {string} method GET | POST | PUT ...
 * @param {object} body objet à envoyer qui sera transformé en json
 * @param {object} options options
 */
export const callApi = async (route, method, body, options = {}) => {
  return co(function*() {
    const headers = { "Content-Type": "application/json", ...options.headers };
    // si on doit envoyer un access token pour cette requête
    if (
      accessToken &&
      (options.useAccessToken === undefined || options.useAccessToken === true)
    ) {
      // si token pas expiré, ou renouvelé avec succès
      if (Date.now() < accessExp || (yield refreshAccessToken())) {
        headers["Authorization"] = `Bearer ${accessToken}`;
      }
    }
    const opts = {
      method,
      body: body ? JSON.stringify(body) : undefined,
      headers
    };
    const response = yield fetch(`/api/${route}`, opts);
    if (!response.ok) {
      const error = new Error(response.statusText);
      error.status = response.status;
      try {
        error.json = yield response.json();
      } catch (err) {
        // nothing to do
      }
      throw error;
    }
    const json = yield response.json();
    return json;
  });
};

/**
 * sauvegarde les tokens d'access et de refresh qui seront utilisé pour les requêtes vers l'API
 *
 * @param {string} access access token
 * @param {string} refresh refresh token
 *
 * @return {object} retourne l'access token décodé, ou false si celui ci n'était pas valide
 */
export const setTokens = (access, refresh) => {
  if (access && refresh) {
    try {
      const decoded = jwt_decode(access);
      accessToken = access;
      refreshToken = refresh;
      accessExp = decoded.exp * 1000;
      localStorage.setItem("access_token", accessToken);
      localStorage.setItem("refresh_token", refreshToken);
      store.dispatch(
        userInfo({
          name: decoded.name,
          email: decoded.email,
          role: decoded.role
        })
      );
      return decoded;
    } catch (error) {
      // error decoding token, it is probably invalid
      store.dispatch(logout("Invalid access token"));
      return false;
    }
  } else {
    const prevRefresh = refreshToken;
    accessToken = refreshToken = null;
    accessExp = 0;
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    if (prevRefresh) {
      post("auth/logout", { token: prevRefresh }, { useAccessToken: false });
    }
    return true;
  }
};

/**
 * initialise l'api
 *
 * @param {Store} store redux store
 * @param {string} access access token (optional)
 * @param {string} refresh refresh token (optional)
 */
export const init = (_store, access, refresh) => {
  store = _store;
  if (access && refresh) {
    store.dispatch(loginSuccess(access, refresh));
  }
};

const Api = {
  get,
  post,
  setTokens,
  init
};

export default Api;
