const MIDIEvents = require("midievents");
const MIDIFile = require("midifile");
const isInteger = require("lodash/isInteger");

const Partition = function() {
  this.header = {};
  this.notes = [];
};

const Note = function(key, time, duration, gain) {
  this.keys = [key];
  this.time = time;
  this.duration = duration;
  this.gain = gain;
  this.control = 0;
};

/**
 * cherche la durée d'une note en trouvant la premiere occurence de note_on ou note_off sur le même channel
 * @param {int} currentEventIndex
 * @param {Array} events
 */
function findDuration(currentEventIndex, events) {
  const curEvent = events[currentEventIndex];
  const channel = curEvent.channel;
  let event;
  for (let i = currentEventIndex + 1; i < events.length; i++) {
    event = events[i];
    /**
     * note off
     */
    if (
      event.channel === channel &&
      (event.subtype === MIDIEvents.EVENT_MIDI_NOTE_OFF ||
        event.subtype === MIDIEvents.EVENT_MIDI_NOTE_ON)
    ) {
      return event.playTime - curEvent.playTime;
    }
  }
  return -1;
}

/**
 * retourne un tableau contenant la liste des notes relativement proches de la note en cours
 * @param {int} currentEventIndex
 * @param {Array} partitionNotes
 */
function nearNotes(currentEventIndex, partitionNotes) {
  const min = Math.max(currentEventIndex - 5, 0);
  let max = Math.min(currentEventIndex + 5, partitionNotes.length);
  let note;
  const notes = [];
  for (let i = min; i < max; i++) {
    note = partitionNotes[i];
    if (notes.indexOf(note.keys[0]) === -1) {
      notes.push(note.keys[0]);
    }
    if (
      i + 1 === max &&
      notes.length < 5 &&
      max < partitionNotes.length &&
      max - min < 20
    ) {
      max += 1;
    }
  }
  return notes.sort();
}

/**
 * construit la partition à partir du fichier midi fourni sous forme de arraybuffer
 * @param {ArrayBuffer} buffer
 * @param {*} options
 */
function build(buffer, options) {
  return new Promise((resolve, reject) => {
    try {
      const midiFile = new MIDIFile(buffer);
      options = Object.assign(
        {},
        // default
        {
          /**
           * Number || Array<Number> || "*"
           */
          channel: "*"
        },
        // user
        options
      );

      let partition = new Partition();

      if (
        midiFile.header.getTimeDivision() === MIDIFile.Header.TICKS_PER_BEAT
      ) {
        partition.header.ticksPerBeat = midiFile.header.getTicksPerBeat();
      } else {
        partition.header.smteFrames = midiFile.header.getSMPTEFrames();
        partition.header.ticksPerFrame = midiFile.header.getTicksPerFrame();
      }

      const events = midiFile.getMidiEvents();
      let lowestKey = Number.MAX_SAFE_INTEGER,
        highestKey = 0;

      /**
       * création de la partition
       */

      const useChannelArray = Array.isArray(options.channel);
      let event, duration, lastNote, gain;
      for (let i = 0; i < events.length; i++) {
        event = events[i];
        /**
         * note on
         */
        if (
          event.subtype === MIDIEvents.EVENT_MIDI_NOTE_ON &&
          (useChannelArray
            ? options.channel.indexOf(event.channel) >= 0
            : event.channel === options.channel || options.channel === "*")
        ) {
          duration = findDuration(i, events);
          gain = event.param2 / 127;
          if (event.param1 < lowestKey) {
            lowestKey = event.param1;
          }
          if (event.param1 > highestKey) {
            highestKey = event.param1;
          }
          if (
            lastNote &&
            lastNote.time === event.playTime &&
            lastNote.keys.indexOf(event.param1) === -1 // &&
            // lastNote.duration == duration
          ) {
            lastNote.keys.push(event.param1);
            if (duration > lastNote.duration) {
              lastNote.duration = duration;
            }
            if (gain > lastNote.gain) {
              lastNote.gain = gain;
            }
          } else if (
            lastNote &&
            lastNote.time === event.playTime &&
            lastNote.keys.indexOf(event.param1) >= 0
          ) {
            if (duration > lastNote.duration) {
              lastNote.duration = duration;
            }
            if (gain > lastNote.gain) {
              lastNote.gain = gain;
            }
          } else {
            lastNote = new Note(event.param1, event.playTime, duration, gain);
            partition.notes.push(lastNote);
          }
        }

        /**
         * errors
         */
        if (lastNote) {
          if (lastNote.duration > 30 * 1000) {
            console.error("note.duration is too long", { lastNote });
            throw new Error("note.duration is too long");
          }
          // on check que la première touche
          if (!isInteger(lastNote.keys[0])) {
            console.error("note.key is not int", { lastNote });
            throw new Error("note.key is not int");
          }
        }
      }

      /**
       * une fois la partoche créée, on repasse dedans pour créer la partition "guitar hero" style
       */
      let note, prev;
      for (let i = 0; i < partition.notes.length; i++) {
        /**
         * on gère pas les multikeys, on prend que la première
         */
        note = partition.notes[i];
        prev = i > 0 ? partition.notes[i - 1] : undefined;
        if (prev && note.keys[0] === prev.keys[0]) {
          note.control = prev.control;
        } else {
          let nears = nearNotes(i, partition.notes);
          let w = nears.indexOf(note.keys[0]);
          let k = Math.round(w / nears.length * 4) + 1;
          if (prev && prev.control === k) {
            if (note.keys[0] > prev.keys[0]) {
              note.control = k + 1;
              while (note.control > 5) {
                note.control -= 5;
              }
            } else {
              note.control = k - 1;
              while (note.control < 1) {
                note.control += 5;
              }
            }
          } else {
            note.control = k;
          }
        }

        /**
         * errors
         */
        if (note.control > 5) {
          console.error("note.control > 5", { note });
          throw new Error("note.control > 5");
        }
        if (note.control < 1) {
          console.error("note.control < 1", { note });
          throw new Error("note.control < 1");
        }
        if (!isInteger(note.control)) {
          console.error("note.control is not int", { note });
          throw new Error("note.control is not int");
        }
      }

      resolve(partition);
    } catch (err) {
      console.error("Error building partition", err);
      reject(err);
    }
  });
}

/**
 * retourne des infos sur le fichier midi
 * @param {ArrayBuffer} buffer
 */
function analyze(buffer) {
  const midiFile = new MIDIFile(buffer);
  const infos = {};

  if (midiFile.header.getTimeDivision() === MIDIFile.Header.TICKS_PER_BEAT) {
    infos.ticksPerBeat = midiFile.header.getTicksPerBeat();
  } else {
    infos.smteFrames = midiFile.header.getSMPTEFrames();
    infos.ticksPerFrame = midiFile.header.getTicksPerFrame();
  }

  const events = midiFile.getMidiEvents();
  const channels = {};

  let event, channel;
  for (let i = 0; i < events.length; i++) {
    event = events[i];
    if (event.subtype === MIDIEvents.EVENT_MIDI_NOTE_ON) {
      channel =
        channels[event.channel] ||
        (channels[event.channel] = {
          id: event.channel,
          numNotes: 0,
          firstTime: event.playTime
        });
      if (channel.lastTime !== event.playTime) {
        channel.numNotes++;
      }
      channel.lastTime = event.playTime;
    }
  }
  infos.channels = channels;

  return infos;
}

module.exports = {
  build,
  analyze
};
