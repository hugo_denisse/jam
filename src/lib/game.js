const PIXI = require("pixi.js");
const co = require("co");
const BinFileLoader = require("./binfileloader");
const PartitionBuilder = require("./partitionbuilder");
const SoundFont = require("soundfont-player");

/**
 * s'incrémente à chaque game jouée
 */
let LAST_GAME_ID = 0;

/**
 * options par défaut de la méthode start
 */
const DEFAULT_START_OPTIONS = {
  backgroundColor: 0x000000,
  soundFont: "acoustic_grand_piano-ogg.js",
  speed: 0.5,
  marge: 150,
  onComplete: undefined
};

/**
 * soundfonts chargées, indexées par nom
 */
const SOUND_FONTS = {};

/**
 * style de la police
 */
const DEFAULT_FONT_STYLE = {
  fontFamily: "Arial",
  fontSize: 20,
  fill: 0xcccccc
};

/**
 * position de la ligne de fin
 */
const FINISHING_LINE_Y = 500;

/**
 * touches
 */
const KEYS = {
  a: 1,
  z: 2,
  e: 3,
  r: 4,
  t: 5
};

/**
 * touches enfoncées
 */
let DOWN_KEYS = {};

/**
 * application PIXI
 */
let app;

/**
 * AudioContext utilisé pour jouer les midis et créer les mp3 buffers
 */
let audioContext;

/**
 * options actives
 */
let options;

/**
 * resources graphiques
 */
let resources;

/**
 * DisplayObjectContainers
 */
let targetsContainer, notesContainer, textsContainer;

/**
 * TextFields
 */
let textScore,
  textScoreValue,
  textStreak,
  textStreakValue,
  textMultip,
  textMultipValue,
  textBestStreak,
  textBestStreakValue,
  textLoading;

/**
 * scores
 */
let score, streak, bestStreak, multip, hit;

/**
 * notes de la partition
 */
let notes, curPlayedNote, lastDisplayedNote;

/**
 * time helpers
 */
let now, startTime, minTime;

/**
 * la partie est-elle finie
 */
let finished;

/**
 * le mp3 a-t-il été démarré
 */
let mp3Started;

/**
 * infos sur la chanson en cours
 */
let curSongInfo;

/**
 * mp3 joué
 */
let audioBufferSource;

/**
 * arguments utilisés pour lancer la partie
 */
let startArgs;

/**
 * id de la game, utilisé pour savoir si la game est terminée
 */
let gameId = 0;

/**
 * lance le chargement des ressources nécessaires puis démarre la partie
 *
 * un canvas sera automatiquement ajouté au container passé en argument
 *
 * @param {object} container élément du DOM auquel sera ajouté le canvas
 * @param {object} songInfo objet contenant les infos de la chanson devant être jouée
 * @param {object} options options
 *
 * songInfo peut avoir les propriétés suivantes :
 * * mp3 {string} nom du fichier mp3 distant à charger
 * * mp3 {AudioBuffer} buffer du mp3 à jouer
 * * midi {string} nom du fichier midi distant à charger
 * * midiBuffer {ArrayBuffer} buffer du midi à jouer / extraire la partition
 * * partition {Partition} la partition à jouer
 *
 * options peut avoir les propriétés suivantes :
 * * soundFont {string} nom de la soundfont à utiliser lors de la lecture du midi
 * * backgroundColor {string} couleur à utiliser pour le fond
 * * speed {number} vitesse de défilement des notes
 * * marge {number} marge d'erreur lors de la frappe sur les touches
 * * onComplete {function} fonction appelée à la fin d'une chanson
 */
function start(container, songInfo, opts) {
  startArgs = [container, songInfo, opts];
  gameId = ++LAST_GAME_ID;
  co(function*() {
    options = { ...DEFAULT_START_OPTIONS, ...opts };

    if (!app) {
      app = new PIXI.Application({ backgroundColor: options.backgroundColor });
      container.appendChild(app.view);
      audioContext = new AudioContext();

      targetsContainer = new PIXI.Container();
      app.stage.addChild(targetsContainer);
      notesContainer = new PIXI.Container();
      app.stage.addChild(notesContainer);
      textsContainer = new PIXI.Container();
      app.stage.addChild(textsContainer);

      textLoading = new PIXI.Text("Loading graphics...", DEFAULT_FONT_STYLE);
      textLoading.x = 20;
      textLoading.y = 20;
      textsContainer.addChild(textLoading);

      resources = yield loadGraphicAssets();
      setupInterface();
    } else {
      container.appendChild(app.view);
      textsContainer.addChild(textLoading);
    }

    textLoading.text = "Loading song...";
    curSongInfo = yield loadSongResources(songInfo, options);

    // si on a appelé stop entre temps
    if (gameId !== LAST_GAME_ID) return;

    notes = curSongInfo.partition.notes;
    textScoreValue.text = "0";
    textStreakValue.text = "0";
    textMultipValue.text = "1";
    textBestStreakValue.text = "0";
    score = 0;
    streak = 0;
    multip = 1;
    bestStreak = 0;
    curPlayedNote = 0;
    minTime = notes[0].time + 10 * 1000;
    lastDisplayedNote = 0;
    hit = 0;
    startTime = now = undefined;
    finished = false;
    mp3Started = false;
    DOWN_KEYS = {};

    document.body.addEventListener("keydown", onKeyDown);
    document.body.addEventListener("keyup", onKeyUp);

    if (songInfo.mp3Buffer) {
      audioBufferSource = audioContext.createBufferSource();
      audioBufferSource.buffer = songInfo.mp3Buffer;
    } else {
      audioBufferSource = null;
    }

    textsContainer.removeChild(textLoading);
    // début dans 3 secondes
    startTime = Date.now() + 3000;
    app.ticker.add(loop);
  });
}

/**
 * lance le chargement des ressources utilisées par la chanson
 *
 * cela peut être mp3, midi, partition, instruments... (dépend des infos contenues dans songInfo)
 *
 * @param {object} songInfo infos de la chanson
 * @param {object} options
 * @return {Promise} retourne songInfo avec des nouveaux champs contenant les ressources chargée
 */
function loadSongResources(songInfo, options) {
  return co(function*() {
    // si la chanson utilise un mp3, mais le mp3Buffer n'est pas encore chargé
    if (songInfo.mp3 && !songInfo.mp3Buffer) {
      const arrayBuffer = yield BinFileLoader.load(songInfo.mp3);
      const audioBuffer = yield audioContext.decodeAudioData(arrayBuffer);
      songInfo.mp3Buffer = audioBuffer;
    }
    // si la chanson possède un midi, mais le midiBuffer n'est pas encore chargé, et que soit pas de mp3 fourni, soit partition pas encore générée
    if (
      songInfo.midi &&
      !songInfo.midiBuffer &&
      (!songInfo.mp3Buffer || !songInfo.partition)
    ) {
      songInfo.midiBuffer = yield BinFileLoader.load(songInfo.midi);
    }
    // si partition pas générée
    if (!songInfo.partition) {
      songInfo.partition = yield PartitionBuilder.build(songInfo.midiBuffer, {
        channel: songInfo.channel || 0
      });
    }
    // si pas de mp3, on va avoir besoin de charger un instrument
    if (!songInfo.mp3Buffer) {
      const sfname = options.soundFont;
      songInfo.soundFontName = sfname;
      if (!SOUND_FONTS[sfname]) {
        SOUND_FONTS[sfname] = yield SoundFont.instrument(
          audioContext,
          `/soundfonts/${sfname}`
        );
      }
    }
    // on retourne songInfo qui contient de nouvelles valeurs
    return songInfo;
  });
}

/**
 * lance le chargement des ressources graphiques
 * @return {Promise} retourne un objet resources qui contient les assets
 */
function loadGraphicAssets() {
  return new Promise((resolve, reject) => {
    PIXI.loader
      .add("note1", "/images/note1.png")
      .add("note2", "/images/note2.png")
      .add("note3", "/images/note3.png")
      .add("note4", "/images/note4.png")
      .add("note5", "/images/note5.png")
      .load((loader, resources) => {
        resolve(resources);
      });
  });
}

/**
 * créer les éléments d'interface nécessaires et les ajoute au stage
 */
function setupInterface() {
  textStreak = new PIXI.Text("Streak:", DEFAULT_FONT_STYLE);
  textStreak.x = 400;
  textStreak.y = 400;
  textsContainer.addChild(textStreak);

  textStreakValue = new PIXI.Text("" + streak, DEFAULT_FONT_STYLE);
  textStreakValue.x = 480;
  textStreakValue.y = 400;
  textsContainer.addChild(textStreakValue);

  textBestStreak = new PIXI.Text("Best:", DEFAULT_FONT_STYLE);
  textBestStreak.x = 550;
  textBestStreak.y = 400;
  textsContainer.addChild(textBestStreak);

  textBestStreakValue = new PIXI.Text("" + bestStreak, DEFAULT_FONT_STYLE);
  textBestStreakValue.x = 620;
  textBestStreakValue.y = 400;
  textsContainer.addChild(textBestStreakValue);

  textMultip = new PIXI.Text("Multip:", DEFAULT_FONT_STYLE);
  textMultip.x = 400;
  textMultip.y = 430;
  textsContainer.addChild(textMultip);

  textMultipValue = new PIXI.Text("" + multip, DEFAULT_FONT_STYLE);
  textMultipValue.x = 480;
  textMultipValue.y = 430;
  textsContainer.addChild(textMultipValue);

  textScore = new PIXI.Text("Score:", DEFAULT_FONT_STYLE);
  textScore.x = 400;
  textScore.y = 480;
  textsContainer.addChild(textScore);

  textScoreValue = new PIXI.Text("" + score, DEFAULT_FONT_STYLE);
  textScoreValue.x = 480;
  textScoreValue.y = 480;
  textsContainer.addChild(textScoreValue);

  for (let i = 1; i <= 5; i++) {
    let sprite = new PIXI.Sprite(resources["note" + i].texture);
    sprite.x = i * 50;
    sprite.y = FINISHING_LINE_Y;
    sprite.anchor.x = sprite.anchor.y = 0.5;
    sprite.alpha = 0.1;
    targetsContainer.addChild(sprite);
  }
}

/**
 * on key down
 * @param {Event} e
 */
function onKeyDown(e) {
  if (e.keyCode !== 116) e.preventDefault(); // on prevent tout sauf F5
  let key = KEYS[e.key];
  if (key) {
    if (DOWN_KEYS[key]) {
      return;
    }
    DOWN_KEYS[key] = true;
    targetsContainer.children[key - 1].alpha = 0.8;

    let search = notesContainer.children.length > 0;
    let success = false;
    let now = Date.now() - startTime;
    let i = 0;
    /**
     * parcours les notes les plus en bas
     */
    while (search) {
      let note = notesContainer.children[i].partitionNote;
      if (
        !note.played &&
        !note.failed &&
        note.control === key &&
        now > note.time - options.marge &&
        now < note.time + options.marge
      ) {
        // playNote(note);
        note.played = true;
        success = true;
        search = false;
      }
      search = search && ++i < notesContainer.children.length;
    }

    if (success) {
      noteSuccess();
    } else {
      noteFail();
    }
  }
}

/**
 * on key up
 * @param {Event} e
 */
function onKeyUp(e) {
  e.preventDefault();
  let key = KEYS[e.key];
  if (key) {
    DOWN_KEYS[key] = false;
    targetsContainer.children[key - 1].alpha = 0.1;
  }
}

/**
 * on note success
 */
function noteSuccess() {
  streak++;
  hit++;
  multip = Math.min(Math.floor(streak / 10) + 1, 4);
  score += 10 * multip;
  textStreakValue.text = "" + streak;
  textMultipValue.text = "" + multip;
  textScoreValue.text = "" + score;
  if (streak > bestStreak) {
    bestStreak = streak;
    textBestStreakValue.text = "" + bestStreak;
  }
}

/**
 * on note fail
 */
function noteFail() {
  streak = 0;
  multip = 1;
  textStreakValue.text = "" + streak;
  textMultipValue.text = "" + multip;
}

/**
 * joue la note avec l'instrument en cours
 * @param {*} note
 */
function playNote(note) {
  for (let key of note.keys) {
    SOUND_FONTS[curSongInfo.soundFontName].play(key, audioContext.currentTime, {
      duration: note.duration > 0 ? note.duration / 1000 : 0.5,
      gain: note.gain
    });
  }
}

/**
 * main loop
 */
function loop() {
  now = Date.now();

  if (
    !mp3Started &&
    audioBufferSource &&
    now >= startTime + (curSongInfo.delay || 0)
  ) {
    audioBufferSource.connect(audioContext.destination);
    audioBufferSource.start(0);
    mp3Started = true;
  }

  /**
   * joue la note en cours
   */
  let note = notes[curPlayedNote];
  while (
    !finished &&
    note.time < now - startTime &&
    curPlayedNote < curPlayedNote + 50
  ) {
    if (!audioBufferSource) {
      playNote(note);
    }
    if (curPlayedNote + 1 >= notes.length) {
      onSongComplete();
      return;
    }
    note = notes[++curPlayedNote];
  }

  /**
   * ajoute les notes devant être affichées
   */
  minTime = notes[curPlayedNote].time + 2 * 1000;
  while (
    lastDisplayedNote < notes.length &&
    minTime > notes[lastDisplayedNote].time
  ) {
    const sprite = new PIXI.Sprite(
      resources["note" + notes[lastDisplayedNote].control].texture
    );
    sprite.alpha = 1;
    sprite.partitionNote = Object.assign({}, notes[lastDisplayedNote]);
    sprite.x = notes[lastDisplayedNote].control * 50;
    sprite.anchor.x = sprite.anchor.y = 0.5;
    notesContainer.addChild(sprite);
    lastDisplayedNote++;
  }

  /**
   * déplace les notes vers le bas et les supprime si elles sont trop loin
   */
  for (let i = notesContainer.children.length - 1; i >= 0; i--) {
    let child = notesContainer.children[i];
    note = child.partitionNote;
    child.y = (-note.time + now - startTime) * options.speed + FINISHING_LINE_Y;
    // si note jouée, on fait un petit effet dessus
    if (note.played) {
      child.alpha -= 0.05;
      child.scale.x = child.scale.y = child.scale.x + 0.05;
      child.y = FINISHING_LINE_Y;
    }
    // note ratée
    if (
      !note.played &&
      !note.failed &&
      now - startTime > note.time + options.marge
    ) {
      note.failed = true;
      noteFail();
    }
    // note à effacer
    if (child.y > FINISHING_LINE_Y + 200 || child.alpha <= 0) {
      notesContainer.removeChild(child);
      child.destroy();
    }
  }
}

/**
 * stoppe la partie
 */
function stop() {
  gameId = 0;
  document.body.removeEventListener("keydown", onKeyDown);
  document.body.removeEventListener("keyup", onKeyUp);
  if (app) {
    app.ticker.remove(loop);
  }
  if (notesContainer) {
    while (notesContainer.children.length) {
      notesContainer.removeChild(notesContainer.children[0]).destroy();
    }
  }
  if (audioBufferSource) {
    if (mp3Started) {
      audioBufferSource.stop(0);
    }
    audioBufferSource = null;
  }
  notes = null;
}

/**
 * redémarre la partie
 */
function restart() {
  stop();
  if (app) start(...startArgs);
}

/**
 * appelé lorsque la dernière note jouable de la chanson est passée
 */
function onSongComplete() {
  finished = true;
  if (options.onComplete) {
    options.onComplete({
      score: score,
      streak: bestStreak,
      notesHit: hit,
      notesTotal: notes.length
    });
  }
}

export default {
  start,
  stop,
  restart
};
