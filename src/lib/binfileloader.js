function load(file) {
  if (typeof file === "string") {
    return loadRemote(file);
  } else if (file instanceof File) {
    return loadLocal(file);
  }
}

function loadRemote(filename) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", filename, true);
    xhr.responseType = "arraybuffer";

    xhr.onload = function() {
      if (this.status !== 200) {
        reject({
          status: this.status,
          message: this.statusText
        });
      }
      resolve(this.response);
    };

    xhr.onerror = function() {
      reject(new Error("Network error"));
    };

    xhr.send();
  });
}

function loadLocal(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = ev => {
      const contents = ev.target.result;
      resolve(contents);
    };
    reader.onerror = err => {
      reject(err);
    };
    reader.readAsArrayBuffer(file);
  });
}

module.exports = {
  load,
  loadLocal,
  loadRemote
};
