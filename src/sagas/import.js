import { takeLatest, put } from "redux-saga/effects";
import {
  IMPORT_MIDI_REQUEST,
  importMidiSuccess,
  importMidiError
} from "../actions/import";
import BinFileLoader from "../lib/binfileloader";
import PartitionBuilder from "../lib/partitionbuilder";

function* importMidiFile(action) {
  const r = yield BinFileLoader.load(action.file)
    .then(buffer => {
      const infos = PartitionBuilder.analyze(buffer);
      return put(importMidiSuccess(buffer, infos));
    })
    .catch(err => {
      return put(importMidiError(err));
    });
  yield r;
}

export default function* importSaga() {
  yield takeLatest(IMPORT_MIDI_REQUEST, importMidiFile);
}
