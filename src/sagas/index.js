import { all, fork } from "redux-saga/effects";
import songs from "./songs";
import imp from "./import";
import login from "./login";

export default function*() {
  yield all([fork(songs), fork(imp), fork(login)]);
}
