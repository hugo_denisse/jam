import { takeLatest, put } from "redux-saga/effects";
import {
  FETCH_SONGS_LIST_REQUEST,
  fetchSongsListError,
  fetchSongsListResponse
} from "../actions/songs";
import {
  FETCH_SONG_META_REQUEST,
  fetchSongMetaResponse,
  fetchSongMetaError
} from "../actions/play";
import Api from "../lib/api";

function* fetchSongsList(action) {
  const r = yield Api.get("songs")
    .then(json => {
      return put(fetchSongsListResponse(json));
    })
    .catch(err => {
      return put(fetchSongsListError(err));
    });
  yield r;
}

function* fetchSongMeta(action) {
  const r = yield Api.get(`song/${action.name}`)
    .then(json => {
      return put(fetchSongMetaResponse(json));
    })
    .catch(err => {
      return put(fetchSongMetaError(err));
    });
  yield r;
}

export default function* songsSaga() {
  yield takeLatest(FETCH_SONGS_LIST_REQUEST, fetchSongsList);
  yield takeLatest(FETCH_SONG_META_REQUEST, fetchSongMeta);
}
