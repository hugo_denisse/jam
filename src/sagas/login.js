import { takeLatest, put } from "redux-saga/effects";
import {
  LOGIN_WITH_PASSWORD_REQUEST,
  LOGIN_SUCCESS,
  REGISTER_REQUEST,
  LOGOUT,
  loginSuccess,
  loginError
} from "../actions/login";
import Api from "../lib/api";

function* loginWithPassword(action) {
  const r = yield Api.post("auth/password", {
    email: action.email,
    password: action.password
  })
    .then(json => {
      return put(loginSuccess(json.access_token, json.refresh_token));
    })
    .catch(err => {
      return put(loginError(err));
    });
  yield r;
}

function* loginResponseSuccess(action) {
  yield Api.setTokens(action.access_token, action.refresh_token);
}

function* register(action) {
  const r = yield Api.post("auth/register", {
    name: action.name,
    email: action.email,
    password: action.password
  })
    .then(json => {
      return put(loginSuccess(json.access_token, json.refresh_token));
    })
    .catch(err => {
      return put(loginError(err));
    });
  yield r;
}

function* logout(action) {
  yield Api.setTokens(null, null);
}

export default function* loginSaga() {
  yield takeLatest(LOGIN_WITH_PASSWORD_REQUEST, loginWithPassword);
  yield takeLatest(LOGIN_SUCCESS, loginResponseSuccess);
  yield takeLatest(REGISTER_REQUEST, register);
  yield takeLatest(LOGOUT, logout);
}
