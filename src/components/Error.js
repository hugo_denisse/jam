import React, { Component } from "react";
import PropTypes from "prop-types";

class Error extends Component {
  render() {
    return (
      <div>
        <p style={style.f}>Une erreur est survenue</p>
        <p style={style.m}>{this.props.error.message}</p>
      </div>
    );
  }
}

const style = {
  f: {
    marginBottom: 0
  },
  m: {
    color: "#343A40"
  }
};

Error.propTypes = {
  error: PropTypes.object.isRequired
};

export default Error;
