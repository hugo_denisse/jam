import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import Play from "../containers/Play";
import Import from "../containers/Import";
import Login from "../containers/Login";
import Register from "../containers/Register";

class Main extends Component {
  render() {
    return (
      <div className="container">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/play/:songname" component={Play} />
          <Route path="/import" component={Import} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route>
            <div>
              <h2>404 - Not Found</h2>
              <p>La page demandée n'a pas été trouvée</p>
            </div>
          </Route>
        </Switch>
      </div>
    );
  }
}

export default Main;
