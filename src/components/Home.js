import React, { Component } from "react";
import Menu from "../containers/Menu";

class Home extends Component {
  render() {
    return (
      <div>
        <Menu />
      </div>
    );
  }
}

export default Home;
