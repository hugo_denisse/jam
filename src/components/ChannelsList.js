import React, { Component } from "react";
import PropTypes from "prop-types";
import { map } from "lodash";

class ChannelsList extends Component {
  componentDidMount() {
    this.selected = [];
  }

  _onCheck(event) {
    if (event.target.checked) {
      this.selected.push(parseInt(event.target.value, 10));
    } else {
      const idx = this.selected.indexOf(parseInt(event.target.value, 10));
      if (idx >= 0) {
        this.selected.splice(idx, 1);
      }
    }
    if (this.props.onChange) {
      this.props.onChange(this.selected);
    }
  }

  render() {
    return (
      <div>
        <label>Selectionner les canaux</label>
        <ul style={style.ul}>
          {map(this.props.channels, (channel, i) => {
            return (
              <li key={i}>
                <input
                  id={"checkbox-channel-" + channel.id}
                  type="checkbox"
                  value={channel.id}
                  onChange={event => this._onCheck(event)}
                />{" "}
                <label htmlFor={"checkbox-channel-" + channel.id}>
                  {channel.id} ~{channel.numNotes} notes (de{" "}
                  {parseInt(channel.firstTime / 1000, 10)}s à{" "}
                  {parseInt(channel.lastTime / 1000, 10)}s)
                </label>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

const style = {
  ul: { listStyleType: "none", padding: 0 }
};

ChannelsList.propTypes = {
  channels: PropTypes.any.isRequired,
  onChange: PropTypes.func
};

export default ChannelsList;
