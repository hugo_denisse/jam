import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="container">
          <p>Jam 1.1.0 - &copy; 2018</p>
        </div>
      </div>
    );
  }
}

export default Footer;
