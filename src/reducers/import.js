import {
  IMPORT_MIDI_REQUEST,
  IMPORT_MIDI_SUCCESS,
  IMPORT_MIDI_ERROR,
  IMPORT_MIDI_OPTIONS,
  IMPORT_MIDI_RESET
} from "../actions/import";

export default function imp(state = {}, action) {
  switch (action.type) {
    // midi
    case IMPORT_MIDI_REQUEST:
      return {
        ...state,
        file: action.file,
        midiBuffer: null,
        midiInfos: null,
        midiOptions: null
      };
    case IMPORT_MIDI_SUCCESS:
      return {
        ...state,
        midiBuffer: action.midiBuffer,
        midiInfos: action.midiInfos,
        midiOptions: null
      };
    case IMPORT_MIDI_ERROR:
      return { ...state, error: action.error };
    case IMPORT_MIDI_OPTIONS:
      return { ...state, midiOptions: action.midiOptions };
    case IMPORT_MIDI_RESET:
      return {};
    // default
    default:
      return state;
  }
}
