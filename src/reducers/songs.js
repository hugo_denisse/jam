import {
  FETCH_SONGS_LIST_REQUEST,
  FETCH_SONGS_LIST_RESPONSE,
  FETCH_SONGS_LIST_ERROR
} from "../actions/songs";

export default function songs(state = {}, action) {
  switch (action.type) {
    // songs list
    case FETCH_SONGS_LIST_REQUEST:
      return { ...state, isFetching: true, error: null };
    case FETCH_SONGS_LIST_RESPONSE:
      return { ...state, isFetching: false, list: action.data, error: null };
    case FETCH_SONGS_LIST_ERROR:
      return { ...state, isFetching: false, error: action.error };
    // default
    default:
      return state;
  }
}
