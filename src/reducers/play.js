import {
  PLAY_SONG_ERROR,
  FETCH_SONG_META_REQUEST,
  FETCH_SONG_META_RESPONSE,
  FETCH_SONG_META_ERROR
} from "../actions/play";

export default function play(state = {}, action) {
  switch (action.type) {
    // play
    case PLAY_SONG_ERROR:
      return { ...state, error: action.error };
    // song meta
    case FETCH_SONG_META_REQUEST:
      return { ...state, isFetching: true, error: null };
    case FETCH_SONG_META_RESPONSE:
      return { ...state, isFetching: false, song: action.data, error: null };
    case FETCH_SONG_META_ERROR:
      return { ...state, isFetching: false, error: action.error };
    // default
    default:
      return state;
  }
}
