import { combineReducers } from "redux";
import songs from "./songs";
import play from "./play";
import imp from "./import";
import login from "./login";

const jamAppReducers = combineReducers({
  // log: (state = {}, action) => {
  //   console.log(action);
  //   return state;
  // },
  songs,
  play,
  import: imp,
  login
});

export default jamAppReducers;
