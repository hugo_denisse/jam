import {
  LOGIN_WITH_PASSWORD_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REGISTER_REQUEST,
  LOGOUT,
  USER_INFO
} from "../actions/login";

export default function login(state = {}, action) {
  switch (action.type) {
    // login
    case LOGIN_WITH_PASSWORD_REQUEST:
      return {
        email: action.email,
        password: action.password
      };
    case LOGIN_SUCCESS:
      return {
        access_token: action.access_token,
        refresh_token: action.refresh_token
      };
    case LOGIN_ERROR:
      return { error: action.error };
    case REGISTER_REQUEST:
      return {
        name: action.name,
        email: action.email,
        password: action.password
      };
    case LOGOUT:
      return {};
    case USER_INFO:
      return {
        access_token: state.access_token,
        refresh_token: state.refresh_token,
        name: action.infos.name,
        email: action.infos.email,
        role: action.infos.role
      };
    // default
    default:
      return state;
  }
}
