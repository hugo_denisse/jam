import "./assets/css/bootstrap.min.css";
import "./index.css";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import App from "./App";
import reducers from "./reducers";
import sagas from "./sagas";
import registerServiceWorker from "./registerServiceWorker";
import Api from "./lib/api";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(sagas);

const access_token = localStorage.getItem("access_token");
const refresh_token = localStorage.getItem("refresh_token");
Api.init(store, access_token, refresh_token);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
